package com.bhushan.tx.sendmail;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.bhushan.tx.dto.FlightBookingAcknowledgement;

/**
 * @author Nitesh_Bhushan
 *
 */
@Service
public class FlightAcknowledgementMail {

	private JavaMailSender javaMailSender;

	@Autowired
	public FlightAcknowledgementMail(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public void sendEmail(FlightBookingAcknowledgement user) throws MailException {

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(user.getPassengerInfo().getEmail());
		mail.setSubject("Hurray Your Ticket Booking has been Confirmed");
		mail.setText(user.getPassengerInfo().getName());

		javaMailSender.send(mail);
	}

	public void sendEmailWithAttachment(FlightBookingAcknowledgement user) throws MailException, MessagingException {

		MimeMessage mimeMessage = javaMailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

		helper.setTo(user.getPassengerInfo().getEmail());
		helper.setSubject("Ticket Booking has been Confirmed");
		helper.setText("Please find the attached Pdf for more Information below.");
		File classPathResource = new File("E:\\TicketInfo.pdf");
		helper.addAttachment(classPathResource.getName(), classPathResource);

		javaMailSender.send(mimeMessage);
	}

}