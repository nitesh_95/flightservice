package com.bhushan.tx.sms;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;

import com.bhushan.tx.dto.FlightBookingAcknowledgement;
/**
 * @author Nitesh_Bhushan
 *
 */
@Service
public class FlightStatusMsg {
	public static HttpResponse sendMsg(FlightBookingAcknowledgement flightBookingAcknowledgement)
			throws ClientProtocolException, IOException {

		String authKey = "****";
		String msg = "Hi  dude  your  Ticket  has  been  booked  having  pnr" + "...." + flightBookingAcknowledgement.getPnrNo()
				+ "...." + "check  your  mail" + "...." + flightBookingAcknowledgement.getPassengerInfo().getEmail()
				+ "...." + "for  more  such  details" + "" + flightBookingAcknowledgement.getStatus()
				+ "Looking  forward  to  serve  you  again";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender  id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + flightBookingAcknowledgement.getPassengerInfo().getMobileno();
		HttpUriRequest httpUriRequest = new HttpGet(url);
		HttpResponse response = client.execute(httpUriRequest);
		return response;

	}
}