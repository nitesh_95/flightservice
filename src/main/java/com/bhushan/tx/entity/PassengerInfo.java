package com.bhushan.tx.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Nitesh_Bhushan
 *
 */

@Entity
@Table(name = "PASSENGER_INFOS")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassengerInfo {
	@Id
	@GeneratedValue
	private Long pid;
	private String name;
	private String email;
	private String source;
	private String destination;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private LocalDate traveldate;
	private String pickuptime;
	private String arrivaltime;
	private double fare;
	private String mobileno;

}