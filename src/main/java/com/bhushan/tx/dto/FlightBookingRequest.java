package com.bhushan.tx.dto;

import com.bhushan.tx.entity.PassengerInfo;
import com.bhushan.tx.entity.PaymentInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Nitesh_Bhushan
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlightBookingRequest {

	private PassengerInfo passengerInfo;

	private PaymentInfo paymentInfo;

}