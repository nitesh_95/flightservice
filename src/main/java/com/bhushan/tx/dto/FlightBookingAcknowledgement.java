package com.bhushan.tx.dto;

import com.bhushan.tx.entity.PassengerInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author Nitesh_Bhushan
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlightBookingAcknowledgement {

	private String status;
	private double totalFare;
	private String pnrNo;
	private PassengerInfo passengerInfo;

}