package com.bhushan.tx.repository;

import com.bhushan.tx.entity.PaymentInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Nitesh_Bhushan
 *
 */
public interface PaymentInfoRepository extends JpaRepository<PaymentInfo,String> {
}
