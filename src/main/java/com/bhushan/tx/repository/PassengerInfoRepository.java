package com.bhushan.tx.repository;

import com.bhushan.tx.entity.PassengerInfo;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * @author Nitesh_Bhushan
 *
 */

public interface PassengerInfoRepository extends JpaRepository<PassengerInfo,Long> {
}
