# spring-transaction-example

RequestBody:-
{
   "passengerInfo":{
      "name":"nitesh",
      "mobileno":"*****",
      "email":"*******@gmail.com",
      "source":"hyd",
      "destination":"hyd",
      "traveldate":"06-01-2021",
      "pickuptime":"4 P.M",
      "arrivaltime":"6 P.M",
      "fare":5000
   },
   "paymentInfo":{
      "accountno":"SBI_CARD",
      "cardtype":"DEBIT"
   }
}

Response:-
----------
{
    "status": "SUCCESS",
    "totalFare": 5000.0,
    "pnrNo": "735d4703",
    "passengerInfo": {
        "pid": 73,
        "name": "nitesh",
        "email": "*********@gmail.com",
        "source": "hyd",
        "destination": "hyd",
        "traveldate": "08-01-2021",
        "pickuptime": "4 P.M",
        "arrivaltime": "6 P.M",
        "fare": 5000.0,
        "mobileno": "********"
    }
}